.. hc_scraper documentation master file, created by
   sphinx-quickstart on Thu Feb 18 01:12:35 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to hc_scraper's documentation!
======================================

.. toctree::
   :maxdepth: 2


Update Chromedriver
===================

.. automodule:: hc_scraper.update.update_chromedriver
   :members:

Scraper
=======

.. automodule:: hc_scraper.scrape.scraper
   :members:
