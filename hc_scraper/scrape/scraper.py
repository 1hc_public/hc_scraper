"""Module to simplify routine webcrawling."""
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities \
    import DesiredCapabilities


def start_driver(path_driver=None, headless=True, test_env=False):
    """
    Initialize webdriver (Chrome).

    Parameters
    ----------
    headless : str, optional
        Path to Chromedriver. The default is under ~/.
    headless : bool, optional
        Whether to run headless or not. The default is True.

    Returns
    -------
    selenium.webdriver.chrome.webdriver.WebDriver
        Webdriver.

    """
    options = webdriver.ChromeOptions()
    if headless:
        options.add_argument('--headless')
        options.add_argument('--single-process')

    if test_env:
        return webdriver.Remote(
            path_driver,
            DesiredCapabilities.CHROME,
            options=options
            )
    else:
        return webdriver.Chrome(
            path_driver,
            options=options
            ) if path_driver else \
            webdriver.Chrome(
                '/home/ubuntu/webdriver/chromedriver',
                options=options
                )
