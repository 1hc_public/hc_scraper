"""Module to download and update latest selenium chromedriver."""
import os
# from io import BytesIO
import requests
# from urllib.request import urlopen
# from zipfile import ZipFile

_PATH_ROOT = os.path.expanduser('~')
PATH_DRIVER = f'{_PATH_ROOT}/webdriver/chromedriver'
PATH_CHROME_URL = ''.join([
    'https://dl.google.com/linux/',
    'direct/google-chrome-stable_current_amd64.deb'
    ])
PATH_URL_ROOT = 'https://chromedriver.storage.googleapis.com'
PATH_URL = f'{PATH_URL_ROOT}/LATEST_RELEASE'


# Update chrome
def update_chrome():
    """
    Update chrome version (debian) to latest.

    Returns
    -------
    None.

    """
    # Check for testing env.
    _r = os.system('sudo pwd')
    if _r != 0:
        os.system('apt-get update')
        os.system('apt-get install apt-utils')
        os.system(' '.join([
            'apt-get install -y',
            'openjdk-11-jre-headless',
            'fonts-liberation',
            'libasound2',
            'libatk-bridge2.0-0',
            'libatk1.0-0',
            'libatspi2.0-0',
            'libcups2',
            'libdbus-1-3',
            'libdrm2',
            'libgbm1',
            'libgtk-3-0',
            'libnspr4',
            'libnss3',
            'libxcomposite1',
            'libxdamage1',
            'libxfixes3',
            'libxkbcommon0',
            'libxrandr2',
            'xdg-utils',
            'xvfb'
            ]))
        os.system('apt-get update')
        os.system(f'wget {PATH_CHROME_URL}')
        os.system(''.join([
            'dpkg --unpack google-chrome-stable_current_amd64.deb',
            ' && apt-get install -f -y'])
            )
        os.remove('google-chrome-stable_current_amd64.deb')
    else:
        os.system('sudo apt-get update')
        os.system('sudo apt-get install apt-utils')
        os.system(' '.join([
            'sudo apt-get install -y',
            'openjdk-11-jre-headless',
            'fonts-liberation',
            'libasound2',
            'libatk-bridge2.0-0',
            'libatk1.0-0',
            'libatspi2.0-0',
            'libcups2',
            'libdbus-1-3',
            'libdrm2',
            'libgbm1',
            'libgtk-3-0',
            'libnspr4',
            'libnss3',
            'libxcomposite1',
            'libxdamage1',
            'libxfixes3',
            'libxkbcommon0',
            'libxrandr2',
            'xdg-utils',
            'xvfb'
            ]))
        os.system('sudo apt-get update')
        os.system(f'wget {PATH_CHROME_URL}')
        os.system(''.join([
            'sudo dpkg --unpack google-chrome-stable_current_amd64.deb',
            ' && sudo apt-get install -f -y'])
            )
        os.system('sudo rm google-chrome-stable_current_amd64.deb')


# Update driver
def clear_path():
    """
    Remove file / create path for driver.

    Returns
    -------
    None.

    """
    # Create driver path
    # if not os.path.exists(f'{_PATH_ROOT}/webdriver'):
    #     os.mkdir(f'{_PATH_ROOT}/webdriver')
    # else:
    #     os.system('rm -rf ~/webdriver')

    # Delete old driver
    if os.path.isfile(PATH_DRIVER):
        os.system(f'rm -rf {PATH_DRIVER}')


# Download webdriver
def download_driver():
    """
    Download latest chromedriver and unzip directly.

    Returns
    -------
    None.

    """
    url_latest = requests.get(PATH_URL).text
    url_chromedriver = ''.join([
        f'{PATH_URL_ROOT}/',
        f'{url_latest}/chromedriver_linux64.zip'
        ])

    # Unzip
    os.system('sudo apt-get install unzip')
    os.system(f'sudo wget {url_chromedriver}')
    os.system('sudo unzip -d ~/webdriver chromedriver_linux64.zip')
    os.system('sudo rm -rf chromedriver_linux64.zip')


# Permission
def permission_file():
    """
    Set necessary permission level for chromedriver.

    Returns
    -------
    None.

    """
    os.system(f'sudo chmod 755 {PATH_DRIVER}')


def update():
    """
    Run full update.

    Returns
    -------
    None.

    """
    update_chrome()
    clear_path()
    download_driver()
    permission_file()
