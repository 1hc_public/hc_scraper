<!-- hc_scraper documentation master file, created by
sphinx-quickstart on Thu Feb 18 01:12:35 2021.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive. -->
# Welcome to hc_scraper’s documentation!

# Update Chromedriver

Module to download and update latest selenium chromedriver.


### hc_scraper.update.update_chromedriver.clear_path()
Remove file / create path for driver.


* **Returns**

    


* **Return type**

    None.



### hc_scraper.update.update_chromedriver.download_driver()
Download latest chromedriver and unzip directly.


* **Returns**

    


* **Return type**

    None.



### hc_scraper.update.update_chromedriver.permission_file()
Set necessary permission level for chromedriver.


* **Returns**

    


* **Return type**

    None.



### hc_scraper.update.update_chromedriver.update()
Run full update.


* **Returns**

    


* **Return type**

    None.



### hc_scraper.update.update_chromedriver.update_chrome()
Update chrome version (debian) to latest.


* **Returns**

    


* **Return type**

    None.


# Scraper

Module to simplify routine webcrawling.


### hc_scraper.scrape.scraper.start_driver(headless=True)
Initialize webdriver (Chrome).


* **Parameters**

    **headless** (*bool**, **optional*) – Whether to run headless or not. The default is True.



* **Returns**

    Webdriver.



* **Return type**

    selenium.webdriver.chrome.webdriver.WebDriver
