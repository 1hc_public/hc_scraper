"""Fee Manager setup."""
from setuptools import setup, find_packages

requirements_path = 'requirements.txt'
with open(requirements_path) as f:
    requirements = f.read().splitlines()

setup(name='hc_scraper',
      version='1.3.5',
      description='Simplifies operations around webscraping.',
      url='https://gitlab.com/1hc_public/1hc_scraper',
      author='1 Howard Capital LLC',
      author_email='support@1howardcapital.com',
      license="Proprietary",
      classifiers=[
          'License :: Other/Proprietary License',
          ],
      packages=find_packages('.',
                             exclude=['tests',
                                      'tests.*',
                                      '*.tests',
                                      '*.tests.*']),
      install_requires=requirements,
      zip_safe=False)
