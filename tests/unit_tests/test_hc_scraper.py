"""Test chromedriver update functionality."""
import unittest
import os

import selenium

from hc_scraper.update import update_chromedriver
from hc_scraper.scrape import scraper

_PATH_ROOT = os.path.expanduser('~')
PATH_DRIVER = f'{_PATH_ROOT}/webdriver/chromedriver'
DOCKER_ENV = os.system('sudo pwd') != 0


class TestUpdateChrome(unittest.TestCase):
    """Test whether chrome updates properly."""

    def test_update_chrome(self):
        """Test whether chrome updates properly."""
        update_chromedriver.update_chrome()
        self.assertTrue(
            True
            )


class TestClearPath(unittest.TestCase):
    """Test whether path cleared correctly."""

    def test_clear_path(self):
        """Test whether path cleared correctly."""
        self.assertEqual(
            update_chromedriver.clear_path(),
            None
            )


class TestDownloadDriver(unittest.TestCase):
    """Test whether chromedriver updates properly."""

    def test_download_driver(self):
        """Test whether chromedriver updates properly."""
        self.assertEqual(
            update_chromedriver.download_driver(),
            None
            )


class TestPermissionFile(unittest.TestCase):
    """Test whether chrome is permissioned properly."""

    def test_permission_file(self):
        """Test whether chrome is permissioned properly."""
        self.assertEqual(
            update_chromedriver.permission_file(),
            None
            )


class TestDriver(unittest.TestCase):
    """Test whether driver starts successfully."""

    def test_driver(self):
        """Test whether driver starts successfully."""
        if not DOCKER_ENV:
            self.assertIsInstance(
                scraper.start_driver(path_driver=PATH_DRIVER),
                selenium.webdriver.chrome.webdriver.WebDriver
                )
        else:
            # Docker environment.
            self.assertIsInstance(
                scraper.start_driver(
                    path_driver='http://selenium__standalone-chrome:4444/wd/hub',
                    test_env=True
                    ),
                selenium.webdriver.remote.webdriver.WebDriver
                )
